SOAP helper
===========

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

To install, either run

```
$ php composer.phar require uhi67/soaphelper "*"
```
or add

```
"uhi67/soaphelper": "*"
```

to the ```require``` section of your `composer.json` file.

## Usage

### create SOAP request XML structure without actually perform the call

```php
    $wsdl = dirname(__DIR__).'/wsdl/my-wsdl.xml'; // WSDL stored in file (or other URI)
    /** @noinspection PhpFullyQualifiedNameUsageInspection */
    $requestXml = \uhi67\soapHelper\SoapClientDry::__requestXml(['a'=>23, 'b'=>49], 'myFunction', $wsdl);
```

## License

**uhi67/soaphelper** is released under the MIT License. See the bundled `LICENSE.md` for details.
