<?php /** @noinspection PhpUnused */

namespace uhi67\soapHelper;

use DomDocument;
use SoapClient;
use SoapFault;

/**
 * Class SoapClientDry
 *
 * The purpose of the class is to create SOAP request XML structure without actually perform the call to the service.
 *
 * @package uhi67\services
 */
class SoapClientDry extends SoapClient
{
	/** @var DomDocument $_request -- the SOAP request DOM of the last fake call */
	private $_request;
	private $_response = '';

	public function __doRequest($request, $location, $action, $version, $oneWay = 0): string {
		$doc = new DomDocument('1.0');
		$doc->preserveWhiteSpace = false;
		$doc->formatOutput = true;
		$doc->loadXML($request);
		$this->_request = $doc;
		// Must return a string to avoid PHP Fatal error. Prepared fake response is returned for
		return $this->_response;
	}

	/**
	 * @param array $parameters
	 * @param string|null $method
	 *
	 * @return DomDocument
	 */
	private function __getRequestXml(array $parameters, $method=null): DomDocument {
		if(!$method) $method = 'fakeCall';
		$this->_response = '';
		$this->__soapCall($method, $parameters);
		return $this->_request;
	}

	/**
	 * @param string $response -- a valid SOAP response XML string
	 * @param array $parameters -- parameters of the fake call (not used)
	 * @param string|null $method
	 *
	 * @return mixed
	 * @noinspection PhpSameParameterValueInspection
	 */
	private function __getResponseValue($response, array $parameters, $method='fakeCall') {
		if(!$method) $method = 'fakeCall';
		$this->_response = $response;
		return $this->__soapCall($method, $parameters);
	}

	/**
	 * Returns SOAP request XML of the not performed call to the given method
	 *
	 * @return string|DomDocument
	 * @throws SoapFault
	 */
	public static function __requestXml($parameters, $method=null, $wsdl=null, $options=null, $asDOM=false) {
		if($options===null)
			/** @noinspection HttpUrlsUsage */
			$options = $wsdl===null ? [
				'location' => "http://localhost/soap.php",
				'uri'      => "http://test-uri/"
			] : [];
		$client = new SoapClientDry($wsdl, $options); // $options must be an array (e.g. empty), null is not valid.
		$request = $client->__getRequestXml($parameters, $method ?? 'fakeCall');
		return $asDOM ? $request : $request->saveXML();
	}

	/**
	 * Returns SOAP request body XML of the not performed call to the given method
	 *
	 * @return string|false
	 * @throws SoapFault
	 */
	public static function __requestBody($parameters, $method=null, $wsdl=null, $options=null) {
		$request = static::__requestXml($parameters, $method, $wsdl, $options, true);
		$bodyNodeList = XmlHelper::xmlQuery('//SOAP-ENV:Body', $request, ['SOAP-ENV'=>"http://schemas.xmlsoap.org/soap/envelope/"]);
		if(!$bodyNodeList || !$bodyNodeList->length) return false;
		$body = $bodyNodeList->item(0);
		return $body->ownerDocument->saveXML($body);
	}

	/**
	 * Returns real value from the SOAP response if response is valid
	 * Returns null on FAULT or invalid response
	 * Throws exception if response is not valid XML.
	 *
	 * @param DomDocument|string $responseXml
	 * @param string $method
	 * @param string $wsdl
	 * @param array $options
	 *
	 * @return mixed
	 * @throws SoapFault
	 */
	public static function __responseValue($responseXml, $method=null, $wsdl=null, $options=null) {
		if($options===null)
			/** @noinspection HttpUrlsUsage */
			$options = $wsdl===null ? [
				'location' => "http://localhost/soap.php",
				'uri'      => "http://test-uri/"
			] : [];
		$client = new SoapClientDry($wsdl, $options); // $options must be an array (e.g. empty), null is not valid.
		if($responseXml instanceof DomDocument) $responseXml = $responseXml->saveXML();
		return $client->__getResponseValue($responseXml, [], $method ?? 'fakeCall');
	}
}
