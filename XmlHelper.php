<?php /** @noinspection PhpUnused */


namespace uhi67\soapHelper;

use DOMDocument;
use DOMNode;
use DOMNodeList;
use DOMXPath;

class XmlHelper
{
    /**
     * Converts string, array or other node to DOMDocument
     *
     * @param DOMDocument|DOMNode|array|string $xml
     * @return DOMDocument|false -- false on failure (invalid xml string)
     */
    public static function toXml($xml)
    {
        if ($xml instanceof DOMDocument) {
            return $xml;
        }
        if (is_array($xml)) {
            return static::arrayToXml($xml);
        }
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        if ($xml instanceof DOMNode) {
            $xml = $dom->importNode($xml, true);
            $dom->appendChild($xml);
            return $dom;
        }

        if (!empty($xml)) {
            $result = $dom->loadXML($xml);
            if(!$result) return false;
        }
        return $dom;
    }

	/**
	 * Evaluates an Xpath query, returns typed result or nodeset
	 *
	 * @param string $query
	 * @param null $xml
	 * @param array $namespaces
	 *
	 * @return DOMNodeList|string|integer|bool -- typed result or nodeset
	 */
    public static function xmlEval(string $query, $xml=null, array $namespaces=[]) {
        $node = $xml instanceof DOMNode ? $xml : null;
        $document = $xml instanceof DOMDocument ? $xml : ($xml instanceof DOMNode ? $xml->ownerDocument : static::toXml($xml));
        $xpath = new DOMXPath($document);
        foreach($namespaces as $prefix=>$namespace) {
            $xpath->registerNamespace($prefix, $namespace);
        }
        return $xpath->evaluate($query, $node);
    }

	/**
	 * Evaluates an Xpath query, returns nodeset
	 *
	 * @param string $query
	 * @param null $xml
	 * @param array $namespaces
	 *
	 * @return DOMNodeList|bool -- nodeset or false on error
	 */
    public static function xmlQuery(string $query, $xml=null, array $namespaces=[]) {
        $node = $xml instanceof DOMNode ? $xml : null;
        $document = $xml instanceof DOMDocument ? $xml : ($xml instanceof DOMNode ? $xml->ownerDocument : static::toXml($xml));
        $xpath = new DOMXPath($document);
        foreach($namespaces as $prefix=>$namespace) {
            $xpath->registerNamespace($prefix, $namespace);
        }
        return $xpath->query($query, $node);
    }

    /**
     * Creates nodes recursively from the array under the specified node.
     * If no parent node is given, a new document will be returned.
     *
     * @param array $array
     * @param DOMNode|string $node
     *
     * @return DOMDocument
     */
    public static function arrayToXml(array $array = [], $node=null): DOMDocument {
        if(!$node || is_string($node)) {
            $xml = new DOMDocument();
            $node = $xml->appendChild($xml->createElement($node ?? 'root'));
        }
        else {
            $xml = $node->ownerDocument;
        }
        foreach ($array as $el => $val) {
            if (is_array($val)) {
                self::arrayToXml($val, $node->$el);
            } else {
                $node->appendChild($xml->createElement($el, $val));
            }
        }
        return $xml;
    }

}